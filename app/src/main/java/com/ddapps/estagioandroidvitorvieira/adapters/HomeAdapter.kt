package com.ddapps.estagioandroidvitorvieira.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.ddapps.estagioandroidvitorvieira.HomeFragmentDirections
import com.ddapps.estagioandroidvitorvieira.R
import com.ddapps.estagioandroidvitorvieira.databinding.RowOffersBinding
import com.ddapps.estagioandroidvitorvieira.models.Offers
import com.squareup.picasso.Picasso

class HomeAdapter(private val offerList: List<Offers>) : RecyclerView.Adapter<HomeAdapter.HomeViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val view: RowOffersBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context!!),
            R.layout.row_offers, parent, false)
        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return offerList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val offer = offerList[position]

        //Carrega imagem da URL
        Picasso.get().load(offer.offer_image).fit().into(holder.offerImage)

        holder.offerTitle.text = offer.offer_name
        holder.offerPrice.text = "R$ ${offer.offer_price}"

        // OnclickListener, no CardView, para navegar ao próximo fragmento com as informações do pacote escolhido
        // serão repassadas com o safe args
        holder.rowCard.setOnClickListener {
            val navController = findNavController(it)
            navController.navigate(HomeFragmentDirections.actionHomeFragmentToOfferFragment(offer.offer_name, offer.offer_price, offer.offer_description, offer.offer_image))
        }
    }

    inner class HomeViewHolder internal constructor(binding: RowOffersBinding) : RecyclerView.ViewHolder(binding.root){
        var rowCard = binding.offerRowCardView
        var offerTitle = binding.offerTitle
        var offerPrice = binding.offerPrice
        var offerImage = binding.offerImage
    }
}