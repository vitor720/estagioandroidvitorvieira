package com.ddapps.estagioandroidvitorvieira


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.UiThread
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ddapps.estagioandroidvitorvieira.adapters.HomeAdapter
import com.ddapps.estagioandroidvitorvieira.databinding.FragmentHomeBinding
import com.ddapps.estagioandroidvitorvieira.models.Offers
import com.ddapps.estagioandroidvitorvieira.retrofit.RetrofitInitializer
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter
import timber.log.Timber


class HomeFragment : Fragment() {

    private lateinit var homeRecycler: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.title = "Viajabessa"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentHomeBinding>(inflater, R.layout.fragment_home, container, false)
        homeRecycler = binding.homeRecycler

        listOffers()

        return binding.root
    }

    private fun listOffers(){

        val call = RetrofitInitializer().offersService().list()

        call.enqueue(object : retrofit2.Callback<List<Offers>?> {

            override fun onResponse(call: retrofit2.Call<List<Offers>?>,
                                    response: retrofit2.Response<List<Offers>?>?) {
                response?.body()?.let {
                    val offers = it
                    Timber.e("Tamanho da lista é: ${offers.size}")
                    configureList(offers)
                }
            }

            override fun onFailure(call: retrofit2.Call<List<Offers>?>?,
                                   t: Throwable?) {
                Toast.makeText(context!!, "FAVOR VERIFICAR SUA CONEXÃO COM A INTERNET", Toast.LENGTH_LONG).show()
                Timber.e("Falhou ${t?.message}")
            }
        })
    }

    private fun configureList(offersList: List<Offers>) {
        homeRecycler.layoutManager = LinearLayoutManager(context!!)
        homeRecycler.adapter  = AlphaInAnimationAdapter(HomeAdapter(offersList))
            .apply {
            setDuration(890)
        }
    }
    override fun onResume() {
        (activity as AppCompatActivity).supportActionBar?.title = "Viajabessa"
        super.onResume()
    }



}
