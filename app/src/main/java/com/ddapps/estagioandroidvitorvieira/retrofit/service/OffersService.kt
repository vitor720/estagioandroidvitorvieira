package com.ddapps.estagioandroidvitorvieira.retrofit.service

import com.ddapps.estagioandroidvitorvieira.models.Offers
import retrofit2.Call
import retrofit2.http.GET

interface OffersService {

    @GET("offers")
    fun list() : Call<List<Offers>>
}