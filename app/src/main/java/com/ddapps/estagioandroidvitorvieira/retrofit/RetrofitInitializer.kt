package com.ddapps.estagioandroidvitorvieira.retrofit

import com.ddapps.estagioandroidvitorvieira.retrofit.service.OffersService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

     private  val retrofit= Retrofit.Builder()
            .baseUrl("https://private-35d8d6-viajabessa720.apiary-mock.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()


    fun offersService () : OffersService{
        return retrofit.create(OffersService::class.java)
    }
}