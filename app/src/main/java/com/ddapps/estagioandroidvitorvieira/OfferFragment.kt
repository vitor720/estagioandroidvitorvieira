package com.ddapps.estagioandroidvitorvieira

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.ddapps.estagioandroidvitorvieira.databinding.FragmentOfferBinding
import com.squareup.picasso.Picasso



class OfferFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val args = OfferFragmentArgs.fromBundle(arguments!!)
        // Inflate the layout for this fragment
        val binding: FragmentOfferBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_offer, container, false)

        val offerName = binding.name
        val offerPrice = binding.price
        val offerDescription= binding.description
        val offerImage = binding.imageView

        Picasso.get().load(args.offerImage).fit().into(offerImage)

        offerName.text = args.offerName
        offerDescription.text = args.offerDescription
        offerPrice.text = "R$ ${args.offerPrice}"


        return binding.root
    }

    override fun onResume() {
        (activity as AppCompatActivity).supportActionBar?.title = "Detalhes do Pacote"
        super.onResume()
    }


}
