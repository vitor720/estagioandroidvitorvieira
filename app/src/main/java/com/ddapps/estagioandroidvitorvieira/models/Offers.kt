package com.ddapps.estagioandroidvitorvieira.models

class Offers (val offer_name: String,
                  val offer_price: Int,
                  val offer_image: String,
                  val offer_description: String)