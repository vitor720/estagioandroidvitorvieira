package com.ddapps.estagioandroidvitorvieira

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import timber.log.Timber

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navController = this.findNavController(R.id.myNavHostFragment)

        NavigationUI.setupActionBarWithNavController(this, navController)


        //Debuger
        Timber.plant(Timber.DebugTree())
    }

    fun setActionBarTitle(title: String) {
        supportActionBar!!.title = title
    }


    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }
}
